var http = require('http');
var myModule = require('./myModule');
http.createServer((req,res)=>{
   res.writeHead(200, {'Content-Type': 'text/html'});
  res.write("The date and time are currently: " + myModule.getDate());
  res.end();
}).listen('8080')