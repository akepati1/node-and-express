const express = require('express')
const mongoose = require('mongoose')
const app = express()
/* env configuration */
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
dotenv.config();

app.set("view engine","ejs");
app.use(bodyParser.urlencoded({extended:true}))
/* mangoose setup and connection */ 
mongoose.connect(process.env.MONGO_URL,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
}).then(()=>console.log("mongodb connection estabilshed"))
.catch((err)=>console.log(err))

/* creating Schema */
const registrationSchema = mongoose.Schema({
    name : {type:String},
    email : {type:String},
    phonoNumber : {type:String},
    address : {type:String},

})
 /* Creating collection also we call as class */
 const userDetails = mongoose.model("userDetails",registrationSchema);
 // It is class with above mention properties inn the schema. so know we 
 // can create objects of this class and push into the dataBase;


 /*-----MiddleWares-----*/
function render(req,res){
         res.render('mongoDb.ejs',{users:users,name:"",email:"",phone:"",address:""});
}

let users = [];
app.get('/',(req,res)=>{
    res.redirect('/users')
  
})
 app.post('/add',async(req,res,next)=>{
     const newUser = userDetails({
         name: req.body.name,
         email:req.body.email,
         phonoNumber : req.body.phoneNumber,
         address  : req.body.address
     })
     const result  = await newUser.save();
     res.redirect('/users')
     next()
 })
app.get('/users',async(req,res, next)=>{
    users = await userDetails.find();
    next()
})
app.get('/edit/:id',async(req,res)=>{
    let id = req.params.id;
    let user   =  await userDetails.findById(id);

    res.render('mongoDb2.ejs',{users:users,id:id,name:user.name,email:user.email,phone:user.phonoNumber,address:user.address})
})
app.post('/save/:id',async(req,res)=>{
    let id = req.params.id;
    let user   =  await userDetails.findById(id);
    // console.log(user)
    // res.render('mongoDb.ejs',{users:users,name:user.name,email:user.email,phone:user.phonoNumber,address:user.address})
    let  newUserData = {
        name: req.body.name,
        email:req.body.email,
        phonoNumber : req.body.phoneNumber,
        address  : req.body.address
    }
    let newuser  = await userDetails.findByIdAndUpdate(id,newUserData);
    console.log(newuser)
     const result = await newuser.save();
    res.redirect('/users')
         
})
app.get('/delete/:id',async(req,res)=>{
    const result = await userDetails.deleteOne({_id : req.params.id})
    res.redirect('/users')
    
})
 app.use(render);

 app.listen('9091')
